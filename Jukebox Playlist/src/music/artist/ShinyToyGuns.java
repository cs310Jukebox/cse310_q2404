package music.artist;
import snhu.jukebox.playlist.Song;
import java.util.ArrayList;
public class ShinyToyGuns {
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public ShinyToyGuns() {
    }
    
    public ArrayList<Song> GetShinyToyGunsTracks() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Le Disko", "ShinyToyGuns");                    //Create a song
         Song track2 = new Song("Chemistry Of A Car Crash", "ShinyToyGuns");
         Song track3 = new Song("Richochet!", "ShinyToyGuns");                  //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for the Beatles
         this.albumTracks.add(track2);  
         this.albumTracks.add(track3);                                          //Add the second song to song list for the Beatles 
         return albumTracks;                                                    //Return the songs for the Beatles in the form of an ArrayList
    }
}
