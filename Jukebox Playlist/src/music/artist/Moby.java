package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class Moby {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Moby() {
    }
    
    public ArrayList<Song> getMobySongs() {
    	
    	 // Instantiate the album so we can populate it below
    	 albumTracks = new ArrayList<Song>();
    	 // Initialize 3 new Songs
    	 Song track1 = new Song("Extreme Ways", "Moby");        		
         Song track2 = new Song("Porcelain", "Moby");         		
         Song track3 = new Song("Flower", "Moby");
         // Add songs to ArrayList
         this.albumTracks.add(track1);                                    
         this.albumTracks.add(track2);
         this.albumTracks.add(track3);
         return albumTracks;                                                    
    }
}
