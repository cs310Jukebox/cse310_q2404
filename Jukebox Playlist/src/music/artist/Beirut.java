package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class Beirut {

	ArrayList<Song> albumTracks;
	String albumTitle;

	public Beirut() {
	}

	public ArrayList<Song> getSongs() {
		albumTracks = new ArrayList<Song>();				// Instantiate the album so we can populate it below

		Song track1 = new Song("Nantes", "Beirut");			// Adding Song1 for Beirut
		Song track2 = new Song("Elephant Gun", "Beirut");	// Adding Song2 for Beirut
		Song track3 = new Song("Gallipoli", "Beirut");		// Adding Song3 for Beirut

		this.albumTracks.add(track1);						// Adding Song1 to Album
		this.albumTracks.add(track2);						// Adding Song2 to Album
		this.albumTracks.add(track3);						// Adding Song3 to Album
	
		return albumTracks;									// Return the songs to the song list
	}
}
