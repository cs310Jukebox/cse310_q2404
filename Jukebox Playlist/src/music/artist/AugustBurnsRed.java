package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class AugustBurnsRed {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public AugustBurnsRed() {
    }
    
    public ArrayList<Song> getABRSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Provision", "August Burns Red");             	//Create a song
         Song track2 = new Song("Treatment", "August Burns Red");         		//Create another song
         Song track3 = new Song("Ghosts", "August Burns Red");					// Add third song
         this.albumTracks.add(track1);                                          //Add the first song to song list for August Burns Red
         this.albumTracks.add(track2);                                          //Add the second song to song list for August Burns Red
         this.albumTracks.add(track3);											//Add the third song to song list for August Burns Red
         return albumTracks;                                                    //Return the songs for August Burns Red in the form of an ArrayList
    }
}
