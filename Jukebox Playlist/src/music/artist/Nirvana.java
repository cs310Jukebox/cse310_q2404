package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class Nirvana {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Nirvana() {
    }
    
    public ArrayList<Song> getNirvanaSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //This instantiates the album so we can populate it below
    	 Song track1 = new Song("Polly", "Nirvana");             				//This creates a song
         Song track2 = new Song("Come as You Are", "Nirvana");         			//This creates another song
         Song track3 = new Song("Smells Like Teen Spirit", "Nirvana");			//This creates another song. Added by: Christian Blosser.
         this.albumTracks.add(track1);                                          //This adds the first song to song list for Nirvana
         this.albumTracks.add(track2);                                          //This adds the second song to song list for Nirvana 
         this.albumTracks.add(track3);											//This adds the third song to song list for Nirvana. Added by: Christian Blosser.
         return albumTracks;                                                    //This returns the songs for Nirvana in the form of an ArrayList
    }
}
