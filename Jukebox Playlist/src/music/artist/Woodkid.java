package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class Woodkid {

	ArrayList<Song> albumTracks;
	String albumTitle;

	public Woodkid() {
	}

	public ArrayList<Song> getSongs() {
		albumTracks = new ArrayList<Song>();				// Instantiate the album so we can populate it below

		Song track1 = new Song("Run Boy Run", "Woodkid");	// Adding Song1 for Woodkid
		Song track2 = new Song("Iron", "Woodkid");			// Adding Song2 for Woodkid
		Song track3 = new Song("Brooklyn", "Woodkid");		// Adding Song3 for Woodkid

		this.albumTracks.add(track1);						// Adding Song1 to Album
		this.albumTracks.add(track2);						// Adding Song2 to Album
		this.albumTracks.add(track3);						// Adding Song3 to Album
	
		return albumTracks;									// Return the songs to the song list
	}
}
