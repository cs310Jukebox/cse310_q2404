package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class LynyrdSkynyrd {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public LynyrdSkynyrd() {
    }
    
    public ArrayList<Song> getLynyrdSkynyrdSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 
    	 //Create songs to add to playlist
    	 Song track1 = new Song("Free Bird", "Lynyrd Skynyrd");             	//(1)Create a song
         Song track2 = new Song("Simple Man", "Lynyrd Skynyrd");				//(2)Create another song
         Song track3 = new Song("Sweet Home Alabama", "Lynyrd Skynyrd");		//(3)Create another song
         Song track4 = new Song("Tuesdays's Gone", "Lynyrd Skynyrd");			//(4)Create another song
         Song track5 = new Song("Gimme Three Steps", "Lynyrd Skynyrd");			//(5)Create another song
         Song track6 = new Song("That Smell", "Lynyrd Skynyrd");				//(6)Create another song
         Song track7 = new Song("The Ballad Of Curtis Loew", "Lynyrd Skynyrd");	//(7)Create another song
         
         //Add created songs to playlist
         this.albumTracks.add(track1);                                          //[1]Add the first song to song list for Lynyrd Skynyrd
         this.albumTracks.add(track2);                                          //[2]Add the second song to song list
         this.albumTracks.add(track3);											//[3]Add the thrid song to song list
         this.albumTracks.add(track4);											//[4]Add song four to song list
         this.albumTracks.add(track5);											//[5]Add song five to song list 
         this.albumTracks.add(track6);											//[6]Add song six to song list
         this.albumTracks.add(track7);											//[7]Add song seven to song list
         
         return albumTracks;                                                    //Return the songs for Lynyrd Skynyrd in the form of an ArrayList
    }
}
