package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class TheCranberries {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public TheCranberries() {
    }
    
    public ArrayList<Song> getCranberriesSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //This instantiates the album so we can populate it below
    	 Song track1 = new Song("Zombie", "The Cranberries");             		//This creates a first song
         Song track2 = new Song("Linger", "The Cranberries");         			//This creates a second song
         Song track3 = new Song("Ode To My Family", "The Cranberries");         //This creates a third song
         this.albumTracks.add(track1);                                          //This adds the first song to song list for the Cranberries
         this.albumTracks.add(track2);                                          //This adds the second song to song list for the Cranberries 
         this.albumTracks.add(track3);                                          //This adds the second song to song list for the Cranberries 
         return albumTracks;                                                    //This returns the songs for the Cranberries in the form of an ArrayList
    }
}
