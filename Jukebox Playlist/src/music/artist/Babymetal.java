package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class Babymetal {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Babymetal() {
    }
    
    public ArrayList<Song> getBabymetalSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("PA PA YA!!", "Babymetal");                     //Create a song
         Song track2 = new Song("Iine", "Babymetal");                           //Create another song
         Song track3 = new Song("MEGITSUNE", "Babymetal");                      //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for the Beatles
         this.albumTracks.add(track2);                                          //Add the second song to song list for the Beatles 
         this.albumTracks.add(track3);                                          //Add the third song to song list for the Beatles 
         return albumTracks;                                                    //Return the songs for the Beatles in the form of an ArrayList
    }
}
