package music.artist;
import snhu.jukebox.playlist.Song;
import java.util.ArrayList;
public class Disturbed {
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Disturbed() {
    }
    
    public ArrayList<Song> GetDisturbedTracks() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Down With The Sickness", "Disturbed");             //Create a song
         Song track2 = new Song("Sound of Silence", "Disturbed");         //Create another song
         Song track3 = new Song("Are You Ready", "Disturbed");         //added by Danielle Obier
         Song track4 = new Song("Warrior", "Disturbed");                   //added by Danielle Obier
         this.albumTracks.add(track1);                                          //Add the first song to song list for the Beatles
         this.albumTracks.add(track2);                                          //Add the second song to song list for the Beatles 
         this.albumTracks.add(track3);                                          //added by Danielle Obier
         this.albumTracks.add(track4);                                          //added by Danielle Obier
         return albumTracks;                                                    //Return the songs for the Beatles in the form of an ArrayList
    }
}
