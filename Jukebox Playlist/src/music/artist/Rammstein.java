package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class Rammstein {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Rammstein() {
    }
    
    public ArrayList<Song> getRammsteinSongs() {
    	
    	 // Instantiate the album so we can populate it below
    	 albumTracks = new ArrayList<Song>();
    	 // Initialize 3 new Songs
    	 Song track1 = new Song("Radio", "Rammstein");        		
         Song track2 = new Song("Deutschland", "Rammstein");         		
         Song track3 = new Song("Stripped", "Rammstein");
         // Add songs to ArrayList
         this.albumTracks.add(track1);                                    
         this.albumTracks.add(track2);
         this.albumTracks.add(track3);
         return albumTracks;                                                    
    }
}
