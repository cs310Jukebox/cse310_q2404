package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class KennyChesney {
	ArrayList<Song> albumTracks;
    String albumTitle;
    //Comment for peer review
    public KennyChesney() {
    }
    
    public ArrayList<Song> getKennyChesneySongs() {
    	
    	 albumTracks = new ArrayList<Song>();                           		//Instantiate the album so we can populate it below
    	 Song track1 = new Song("Don't Blink", "Kenny Chesney");         		//Create first song
         Song track2 = new Song("There Goes My Life", "Kenny Chesney");      	//Create second song
         Song track3 = new Song("Somewhere With You", "Kenny Chesney");     	//Create third song
         Song track4 = new Song("El Cerrito Place", "Kenny Chesney");			//Create fourth song. Added by: Christian Blosser.
         Song track5 = new Song("Live a Little", "Kenny Chesney");				//Create fifth song. Added by: Christian Blosser.
         Song track6 = new Song("The Good Stuff", "Kenny Chesney");				//Create sixth song. Added by: Christian Blosser.
         Song track7 = new Song("Keg in the Closet", "Kenny Chesney");			//Create 7th song. Added by: Christian Blosser.
         this.albumTracks.add(track1);                                  		//Add the first song to song list
         this.albumTracks.add(track2);                                  		//Add the second song to song list
         this.albumTracks.add(track3);                                  		//Add the third song to song list
         this.albumTracks.add(track4);											//Add the fourth song to song list. Added by: Christian Blosser.
         this.albumTracks.add(track5);											//Add the fifth song to song list. Added by: Christian Blosser.
         this.albumTracks.add(track6);											//Add the sixth song to song list. Added by: Christian Blosser.
         this.albumTracks.add(track7);											//Add the 7th song to song list. Added by: Christian Blosser.
         return albumTracks;                                            		//Return the songs for Kenny Chesney in the form of an ArrayList
    }
}
