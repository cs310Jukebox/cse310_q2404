package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

/**
 * A collection of Rick Astley songs 
 */
public class RickAstley {

	ArrayList<Song> albumTracks;
	String albumTitle;

	public RickAstley() {
	}

	/**
	 * Returns an album of Rick Astley songs
	 * 
	 * @return The songs for Rick Astley in the form of an ArrayList 
	 */
	public ArrayList<Song> getSongs() {

		// Instantiate the album so we can populate it below
		albumTracks = new ArrayList<Song>();

		// Create songs
		Song track1 = new Song("Never Gonna Give You Up", "Rick Astley");

		// Add songs to the song list
		this.albumTracks.add(track1);

		// Return the songs in the form of an ArrayList
		return albumTracks;
	}
}
