package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

/**
 * @author LoganDuck
 */
public class RoyWoods {
	
	ArrayList<Song> albumTracks;
	String albumTitle;
	
	public RoyWoods() {}
	
	public ArrayList<Song> getSongs() {
		albumTracks = new ArrayList<Song>();
		Song track1 = new Song("How I Feel", "Roy Woods");
		Song track2 = new Song("Say Less", "Roy Woods");
		Song track3 = new Song("Get You Good", "Roy Woods");
		this.albumTracks.add(track1);
		this.albumTracks.add(track2);
		this.albumTracks.add(track3);
		return albumTracks;
	}
	
	public int getSize() {
		return this.albumTracks.size();
	}
}