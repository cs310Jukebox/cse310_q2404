package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

/**
 * A collection of Michael Jackson songs 
 */
public class MichaelJackson {

	ArrayList<Song> albumTracks;
	String albumTitle;

	public MichaelJackson() {
	}

	/**
	 * Returns an album of Michael Jackson songs
	 * 
	 * @return The songs for Michael Jackson in the form of an ArrayList 
	 */
	public ArrayList<Song> getSongs() {

		// Instantiate the album so we can populate it below
		albumTracks = new ArrayList<Song>();

		// Create songs
		Song track1 = new Song("Thriller", "Michael Jackson");
		Song track2 = new Song("Beat it", "Michael Jackson");
		Song track3 = new Song("Billie Jean", "Michael Jackson");
		Song track4 = new Song("You Are Not Alone", "Michael Jackson");
		Song track5 = new Song("Man In The Mirror", "Michael Jackson"); // Added by Matthew McEntire

		// Add songs to the song list
		this.albumTracks.add(track1);
		this.albumTracks.add(track2);
		this.albumTracks.add(track3);
		this.albumTracks.add(track4);
		this.albumTracks.add(track5); //Added by Matthew McEntire

		// Return the songs in the form of an ArrayList
		return albumTracks;
	}
}
