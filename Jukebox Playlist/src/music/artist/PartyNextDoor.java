package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

/**
 * @author LoganDuck
 */
public class PartyNextDoor {
	
	ArrayList<Song> albumTracks;
	String albumTitle;
	
	public PartyNextDoor() {}
	
	public ArrayList<Song> getSongs() {
		albumTracks = new ArrayList<Song>();
		Song track1 = new Song("Rendezvous", "PartyNextDoor");
		Song track2 = new Song("Come and See Me", "PartyNextDoor");
		Song track3 = new Song("Peace of Mind", "PartyNextDoor");
		this.albumTracks.add(track1);
		this.albumTracks.add(track2);
		this.albumTracks.add(track3);
		return albumTracks;
	}
	
	public int getSize() {
		return this.albumTracks.size();
	}
}