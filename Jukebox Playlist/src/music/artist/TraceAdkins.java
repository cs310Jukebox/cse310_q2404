package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class TraceAdkins {
	ArrayList<Song> albumTracks;
    String albumTitle;
    //Comment to submit for peer review.
    
    public TraceAdkins() {
    }
    
    public ArrayList<Song> getTraceAdkinsSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                           		//Instantiate the album so we can populate it below
    	 Song track1 = new Song("Honky Tonk Badonkadonk", "Trace Adkins");         		//Create first song
         Song track2 = new Song("You're Gonna Miss This", "Trace Adkins");      	//Create second song
         Song track3 = new Song("Arlington", "Trace Adkins");     	//Create third song
         this.albumTracks.add(track1);                                  		//Add the first song to song list
         this.albumTracks.add(track2);                                  		//Add the second song to song list
         this.albumTracks.add(track3);                                  		//Add the third song to song list
         return albumTracks;                                            		//Return the songs for Trace Adkins in the form of an ArrayList
    }
}
