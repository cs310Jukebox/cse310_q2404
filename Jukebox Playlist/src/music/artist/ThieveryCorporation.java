/* New play list added by Robert Clark 30NOV19
 * included songs from Thievery Corporation. Each can be located here:
 * Lebanese Blonde : https://www.youtube.com/watch?time_continue=6&v=04bg9IC9N6w&feature=emb_title
 * La Femme Parallel : https://www.youtube.com/watch?v=h7tDWA9PfGg
 * Letter to the editor : https://www.youtube.com/watch?v=KahPH6oc12I
 * Enjoy :).
 */

package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class ThieveryCorporation { //added band name for new class

	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public ThieveryCorporation() { 
    }
    
    public ArrayList<Song> getThieveryCorporationSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                           //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Lebanese Blonde", "Thievery Corporation");         //Create a song
         Song track2 = new Song("La Femme Parallel", "Thievery Corporation");      //Create another song
         Song track3 = new Song("Letter to the Editor", "Thievery Corporation");           //Create a third song
         this.albumTracks.add(track1);                                  //Add the first song to song list for Thievery Corporation
         this.albumTracks.add(track2);                                  //Add the second song to song list for Thievery Corporation 
         this.albumTracks.add(track3);                                  //Add the third song to song list for Thievery Corporation 
         return albumTracks;                                            //Return the songs for Thievery Corporation in the form of an ArrayList
    }
}

