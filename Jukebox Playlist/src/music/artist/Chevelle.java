package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class Chevelle {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Chevelle() {
    }
    
    public ArrayList<Song> getChevelleSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album
    	 Song track1 = new Song("Face to the Floor", "Chevelle");             //Create a song
         Song track2 = new Song("Hunter Eats Hunter", "Chevelle");         //Create another song
         Song track3 = new Song("Send the Pain Below", "Chevelle");         //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Chevelle
         this.albumTracks.add(track2);                                          //Add the second song to song list for Chevelle
         this.albumTracks.add(track3);
         return albumTracks;                                                    //Return the songs for Chevelle in the form of an ArrayList
    }
}