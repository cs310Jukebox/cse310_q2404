package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class Red {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Red() {
    }
    
    public ArrayList<Song> getRedSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Darkest Part", "Red");             			//Create a song
         Song track2 = new Song("Buried Beneath", "Red");         				//Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list Red
         this.albumTracks.add(track2);                                          //Add the second song to song list for Red
         return albumTracks;                                                    //Return the songs for August Burns Red in the form of an ArrayList
    }
}
