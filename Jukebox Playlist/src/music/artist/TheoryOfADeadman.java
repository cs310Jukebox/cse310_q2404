package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class TheoryOfADeadman {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public TheoryOfADeadman() {
    }
    
    public ArrayList<Song> getTheoryOfADeadmanSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Rx (Medicate)", "Theory Of A Deadman");        //Create a song
         Song track2 = new Song("Vilain", "Theory Of A Deadman");   			//Create another song
         Song track3 = new Song("Hate My Life", "Theory Of A Deadman"); 		//Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Theory Of A Deadman
         this.albumTracks.add(track2);                                          //Add the second song to song list for Theory Of A Deadman 
         this.albumTracks.add(track3);											//Add the third song to song list for Theory Of A Deadman
         return albumTracks;                                                    //Return the songs for Theory Of A Deadman in the form of an ArrayList
    }
}