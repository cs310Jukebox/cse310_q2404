package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class JoshTurner {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public JoshTurner() {
    }
    
    public ArrayList<Song> getJoshTurnerSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 
    	 //Create songs to add to playlist
    	 Song track1 = new Song("Why Don't We Just Dance", "Josh Turner");     	//(1)Create a song
         Song track2 = new Song("Lay Low", "Josh Turner");						//(2)Create another song
         Song track3 = new Song("Hometown Girl", "Josh Turner");				//(3)Create another song
         Song track4 = new Song("All About You", "Josh Turner");				//(4)Create another song
         Song track5 = new Song("Deep South", "Josh Turner");					//(5)Create another song
         Song track6 = new Song("Firecracker", "Josh Turner");					//(6)Create another song
         Song track7 = new Song("Long Black Train", "Josh Turner");				//(7)Create another song
         
         //Add created songs to playlist
         this.albumTracks.add(track1);                                          //[1]Add the first song to song list for Josh Turner
         this.albumTracks.add(track2);                                          //[2]Add the second song to song list
         this.albumTracks.add(track3);											//[3]Add the thrid song to song list
         this.albumTracks.add(track4);											//[4]Add song four to song list
         this.albumTracks.add(track5);											//[5]Add song five to song list 
         this.albumTracks.add(track6);											//[6]Add song six to song list
         this.albumTracks.add(track7);											//[7]Add song seven to song list
         
         return albumTracks;                                                    //Return the songs for Josh Turner in the form of an ArrayList
    }
}
