package snhu.jukebox.playlist.tests;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import snhu.jukebox.playlist.Student;
import snhu.jukebox.playlist.StudentList;
import snhu.student.playlists.*;

public class StudentTest {

	//Test the list of student names and ensure we get back the name value we expect at the correct/specific point in the list
	@Test
	public void testGetStudentNameList1() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("TestStudent1Name", studentNames.get(0));							//test case for pass/fail. We expect the first name to be TestStudent1Name. Remember arrays start their count at 0 not 1.
	}
	
	@Test
	public void testGetStudentNameList2() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("TestStudent2Name", studentNames.get(1));							//test case to see if the second value contains the name we expect
	}
	
	@Test
	public void testGetStudentNameList3() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertTrue(studentNames.contains("Jamie"));							            //test case to see if the third value contains the name we expect
	}
	
	public void testGetStudentnameSubash() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertEquals("Subash", studentNames.contains("Subash"));									//test case to see if index 2 has the name Subash
	}
	
	@Test
	public void testGetStudentNameDavidMcWilliams() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertTrue(studentNames.contains("David McWilliams"));							//test case to see if index 3 contains the name we expect
	}
	
	@Test
	public void testGetStudentNameRobertClark() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertTrue(studentNames.contains("Robert Clark"));							//test case to see if index 4 contains the name we expect
	}
	
	@Test
	public void testGetStudentNameBryceZimbelman() {
		List<String> studentNames = new ArrayList<String>();							
		StudentList studentList = new StudentList();									
		studentNames = studentList.getStudentsNames();									
		assertTrue(studentNames.contains("Bryce Zimbelman"));
	}
	
	@Test
	public void testGetStudentNameSergeyRebbe() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertTrue(studentNames.contains("Sergey Rebbe"));							    //test case for pass/fail. "Sergey Rebbe" is expected
	}

	//Added test case for student: Jeff Chouiniere
	@Test
	public void testGetJeffChouiniere() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertTrue(studentNames.contains("Jeff Chouiniere"));							//contains ensures we getting a passing test each time.
	}
	
	//Add test case to check creation of student Christian Blosser
	@Test
	public void testGetChristianBlosser() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertTrue(studentNames.contains("Christian Blosser"));							//contains ensures we getting a passing test each time.
	}

	//Module 5 - Add your unit test case here to check for your name after you have added it to the StudentList

	
		
		
	//Module 6 Test Case Area
	//Test each student profile to ensure it can be retrieved and accessed
	//Add your unit test case here to check for your profile after you have added it to the StudentList
	
	@Test
	public void testGetStudent1Profile() {
		TestStudent1_Playlist testStudent1Playlist = new TestStudent1_Playlist();						//instantiating the variable for a specific student
		Student TestStudent1 = new Student("TestStudent1", testStudent1Playlist.StudentPlaylist());		//creating populated student object
		assertEquals("TestStudent1", TestStudent1.getName());											//test case pass/fail line - did the name match what you expected?
	}
	
	@Test
	public void testGetStudent2Profile() {
		TestStudent2_Playlist testStudent2Playlist = new TestStudent2_Playlist();
		Student TestStudent2 = new Student("TestStudent2", testStudent2Playlist.StudentPlaylist());
		assertEquals("TestStudent2", TestStudent2.getName());
	}
	
	@Test
	public void testGetJamieProfile() {                                                                 //instantiating the variable for a specific student
		Jamie_Playlist testJamiePlaylist = new Jamie_Playlist();                                        //creating populated student object
		Student TestJamie = new Student("Jamie", testJamiePlaylist.StudentPlaylist());                  //test case pass/fail line - did the name match what you expected?
		assertEquals("Jamie", TestJamie.getName());
	}
	
	@Test
	public void TestGetMatthewMcEntireProfile() {
		List<String> studentNames = new ArrayList<String>();							//create variable for student list of names
		StudentList studentList = new StudentList();									//instantiate the StudentList object so we can access it's methods and properties
		studentNames = studentList.getStudentsNames();									//populate the studentNames list with the actual values in the StudentsList object
		assertTrue(studentNames.contains("Matthew McEntire"));							//test case for pass/fail. We expect the first name to be TestStudent1Name. Remember arrays start their count at 0 not 1.
	}
		
	@Test
	public void testGetRobertClarkProfile() {
		RobertClark_Playlist testRobertClarkPlaylist = new RobertClark_Playlist();						//instantiating the variable for a specific student
		Student TestRobertClark = new Student("Robert Clark", testRobertClarkPlaylist.StudentPlaylist());		//creating populated student object
		assertEquals("Robert Clark", TestRobertClark.getName());											//test case pass/fail line - did the name match what you expected?
	}
	
	@Test
	public void testGetBryceZimbelmanProfile() {
		BryceZimbelman_Playlist testBryceZimbelmanPlaylist = new BryceZimbelman_Playlist();						
		Student TestBryceZimbelman = new Student("Bryce Zimbelman", testBryceZimbelmanPlaylist.StudentPlaylist());		
		assertEquals("Bryce Zimbelman", TestBryceZimbelman.getName());											
	}

	@Test
	public void testGetAndrewTroupProfile() throws NoSuchFieldException, SecurityException {
		AndrewTroup_Playlist andrewTroup_Playlist = new AndrewTroup_Playlist();
		Student AndrewTroup = new Student("Andrew Troup", andrewTroup_Playlist.StudentPlaylist());
		assertEquals("Andrew Troup", AndrewTroup.getName());
	}
		
	@Test
	public void testGetSubashProfile() {
		Subash_Playlist testSubashPlaylist = new Subash_Playlist();							//instantiating the variable for a specific student
		Student TestSubash = new Student("Subash", testSubashPlaylist.StudentPlaylist());	//creating populated student object
		assertEquals("Subash", TestSubash.getName());										//test case pass/fail line - did the name match Subash as expected?
	}

	@Test
	public void testGetDavidMcWilliamsProfile() {
		// Create the playlist object
		DavidMcWilliams_Playlist playlist = new DavidMcWilliams_Playlist();						
		// Create the student profile
		Student student = new Student("David McWilliams", playlist.StudentPlaylist());
		// Pass/fail line - did the name match what we expected?
		assertEquals("David McWilliams", student.getName());											//test case pass/fail line - did the name match what you expected?
	}
	
	@Test
	public void testChristianBlosserProfile() {
		ChristianBlosser_Playlist testChristianBlosserPlaylist = new ChristianBlosser_Playlist();
		Student TestChristianBlosser = new Student("Christian Blosser", testChristianBlosserPlaylist.StudentPlaylist());
		assertEquals("Christian Blosser", TestChristianBlosser.getName());	
	}
	//Module 6 - Add your unit test case here to check for your profile after you have added it to the StudentList
}
