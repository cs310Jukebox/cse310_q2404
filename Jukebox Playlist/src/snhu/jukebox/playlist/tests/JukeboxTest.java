package snhu.jukebox.playlist.tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import music.artist.Adele;
import music.artist.AugustBurnsRed;
import music.artist.Disturbed;
import music.artist.ImagineDragons;
import music.artist.JoshTurner;
import music.artist.LynyrdSkynyrd;
import music.artist.MichaelJackson;
import music.artist.Moby;
import music.artist.PartyNextDoor;
import music.artist.Rammstein;
import music.artist.Red;
import music.artist.RickAstley;
import music.artist.RoyWoods;
import music.artist.ShinyToyGuns;
import music.artist.TheBeatles;
import music.artist.ThieveryCorporation;
import music.artist.Nirvana;
import music.artist.TheCranberries;
import snhu.jukebox.playlist.Song;

public class JukeboxTest {

	@Test
	public void testGetBeatlesAlbumSize() throws NoSuchFieldException, SecurityException {
		 TheBeatles theBeatlesBand = new TheBeatles();
		 ArrayList<Song> beatlesTracks = new ArrayList<Song>();
		 beatlesTracks = theBeatlesBand.getBeatlesSongs();
		 assertEquals(2, beatlesTracks.size());
	}
	
	@Test
	public void TestMatthewMcEntireDisturbedPlayList() throws NoSuchFieldException, SecurityException {
		 Disturbed disturbed = new Disturbed();
		 ArrayList<Song> disturbedTracks = new ArrayList<Song>();
		 disturbedTracks = disturbed.GetDisturbedTracks();
		 assertEquals(4, disturbedTracks.size());
	}
	
	@Test
	public void TestMatthewMcEntireShinyToyGunsPlayList() throws NoSuchFieldException, SecurityException {
		 ShinyToyGuns shinyToyGuns = new ShinyToyGuns();
		 ArrayList<Song> shinyToyGunsTracks = new ArrayList<Song>();
		 shinyToyGunsTracks = shinyToyGuns.GetShinyToyGunsTracks();
		 assertEquals(3, shinyToyGunsTracks.size());
	}
	
	@Test
	public void testGetImagineDragonsAlbumSize() throws NoSuchFieldException, SecurityException {
		 ImagineDragons imagineDragons = new ImagineDragons();
		 ArrayList<Song> imagineDragonsTracks = new ArrayList<Song>();
		 imagineDragonsTracks = imagineDragons.getImagineDragonsSongs();
		 assertEquals(4, imagineDragonsTracks.size());
	}
	
	@Test
	public void testGetAdelesAlbumSize() throws NoSuchFieldException, SecurityException {
		 Adele adele = new Adele();
		 ArrayList<Song> adelesTracks = new ArrayList<Song>();
		 adelesTracks = adele.getAdelesSongs();
		 assertEquals(3, adelesTracks.size());
	}

	@Test
	public void testGetMichaelJacksonAlbumSize() throws NoSuchFieldException, SecurityException {
		 MichaelJackson album = new MichaelJackson();
		 ArrayList<Song> tracks = new ArrayList<Song>();
		 tracks = album.getSongs();
		 assertEquals(5, tracks.size());
	}

	@Test
	public void testGetRickAstleyAlbumSize() throws NoSuchFieldException, SecurityException {
		 RickAstley album = new RickAstley();
		 ArrayList<Song> tracks = new ArrayList<Song>();
		 tracks = album.getSongs();
		 assertEquals(1, tracks.size());
	}
	
	@Test  //added Thievery Corporation 30DEC19 Robert Clark
	public void testGetThieveryCorporationAlbumSize() throws NoSuchFieldException, SecurityException {
		 ThieveryCorporation ThieveryCorporationBand = new ThieveryCorporation();
		 ArrayList<Song> ThieveryCorporationTracks = new ArrayList<Song>();
		 ThieveryCorporationTracks = ThieveryCorporationBand.getThieveryCorporationSongs();
		 assertEquals(3, ThieveryCorporationTracks.size());
	}
	
	@Test
	public void testGetABRAlbumSize() throws NoSuchFieldException, SecurityException {
		 AugustBurnsRed ABRBand = new AugustBurnsRed();
		 ArrayList<Song> ABRTracks = new ArrayList<Song>();
		 ABRTracks = ABRBand.getABRSongs();
		 assertEquals(3, ABRTracks.size());
	}
	
	@Test
	public void testGetRedAlbumSize() throws NoSuchFieldException, SecurityException {
		 Red redBand = new Red();
		 ArrayList<Song> redTracks = new ArrayList<Song>();
		 redTracks = redBand.getRedSongs();
		 assertEquals(2, redTracks.size());
	}
	
	@Test
	public void testGetRammsteinAlbumSize() throws NoSuchFieldException, SecurityException {
		 Rammstein rammstein = new Rammstein();
		 ArrayList<Song> rammsteinTracks = new ArrayList<Song>();
		 rammsteinTracks = rammstein.getRammsteinSongs();
		 assertEquals(3, rammsteinTracks.size());
	}
	
	@Test
	public void testGetMobyAlbumSize() throws NoSuchFieldException, SecurityException {
		 Moby moby = new Moby();
		 ArrayList<Song> mobyTracks = new ArrayList<Song>();
		 mobyTracks = moby.getMobySongs();
		 assertEquals(3, mobyTracks.size());
	}
	

	@Test
	public void testPartyNextDoorSize() {
		PartyNextDoor pnd = new PartyNextDoor();
		ArrayList<Song> tracks = pnd.getSongs();
		assertEquals(3, tracks.size());
	}
	
	@Test
	public void testRoyWoodsSize() {
		RoyWoods roy = new RoyWoods();
		ArrayList<Song> tracks = roy.getSongs();
		assertEquals(3, tracks.size());
	}
	// test added by Subash for two bands chosen
	@Test
	public void testGetNirvanaAlbumSize() throws NoSuchFieldException, SecurityException {
		 Nirvana nirvanaBand = new Nirvana();
		 ArrayList<Song> nirvanaTracks = new ArrayList<Song>();
		 nirvanaTracks = nirvanaBand.getNirvanaSongs();
		 assertEquals(3, nirvanaTracks.size()); //changed assert to 3, added song to Nirvana
	}
	
	@Test
	public void testGetCranberriesAlbumSize() throws NoSuchFieldException, SecurityException {
		 TheCranberries theCranberriesBand = new TheCranberries();
		 ArrayList<Song> cranberriesTracks = new ArrayList<Song>();
		 cranberriesTracks = theCranberriesBand.getCranberriesSongs();
		 assertEquals(3, cranberriesTracks.size());
	}
	
	@Test
	public void testLynyrdSkynrdAlbumSize() throws NoSuchFieldException, SecurityException {
		 LynyrdSkynyrd LynyrdSkynyrdBand = new LynyrdSkynyrd();
		 ArrayList<Song> LynyrdSkynyrdTracks = new ArrayList<Song>();
		 LynyrdSkynyrdTracks = LynyrdSkynyrdBand.getLynyrdSkynyrdSongs();
		 assertEquals(7, LynyrdSkynyrdTracks.size());
	}
	
	@Test
	public void testJoshTurnerAlbumSize() throws NoSuchFieldException, SecurityException {
		 JoshTurner JoshTurnerBand = new JoshTurner();
		 ArrayList<Song> JoshTurnerTracks = new ArrayList<Song>();
		 JoshTurnerTracks = JoshTurnerBand.getJoshTurnerSongs();
		 assertEquals(7, JoshTurnerTracks.size());
	}	

}
