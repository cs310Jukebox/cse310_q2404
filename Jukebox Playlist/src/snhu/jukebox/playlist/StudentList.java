package snhu.jukebox.playlist;

import java.util.ArrayList;
import java.util.List;

import snhu.student.playlists.AndrewTroup_Playlist;
import snhu.student.playlists.BryceZimbelman_Playlist;
import snhu.student.playlists.ChristianBlosser_Playlist;
import snhu.student.playlists.ChristopherMinton_Playlist;
import snhu.student.playlists.DavidMcWilliams_Playlist;
import snhu.student.playlists.Jamie_Playlist;
import snhu.student.playlists.KennethBorges_Playlist;
import snhu.student.playlists.LoganDuck_Playlist;
import snhu.student.playlists.MatthewMcEntire_PlayList;
import snhu.student.playlists.RobertClark_Playlist;
import snhu.student.playlists.Subash_Playlist;
import snhu.student.playlists.TestStudent1_Playlist;
import snhu.student.playlists.TestStudent2_Playlist;

public class StudentList {

	public StudentList(){
	}

	public List<String> getStudentsNames() {
		ArrayList<String> studentNames = new ArrayList<String>();
		
		String StudentName1 = "TestStudent1Name";
		studentNames.add(StudentName1);
		
		String StudentName2 = "TestStudent2Name";
		studentNames.add(StudentName2);
		
		//Module 5 Code Assignment
		//Add your name to create a new student profile
		//Use template below and put your name in the areas of 'StudentName'
		//String StudentName3 = "TestStudent3Name";
		//studentNames.add(StudentName3);
		
		String Subash = "Subash"; // avoiding full name to avoid confusions about spelling and spacing
		studentNames.add(Subash);
		
		String MatthewMcEntire = "Matthew McEntire";
		studentNames.add(MatthewMcEntire);
		
		String StudentNameDavidMcWilliams = "David McWilliams";
		studentNames.add(StudentNameDavidMcWilliams);
		
		String StudentNameRobertClark = "Robert Clark";
		studentNames.add(StudentNameRobertClark);
		
		String StudentNameBryceZimbelman = "Bryce Zimbelman";
		studentNames.add(StudentNameBryceZimbelman);
		
		String StudentNameSergeyRebbe = "Sergey Rebbe";
		studentNames.add(StudentNameSergeyRebbe);
		
		//Adding Student: Jeff Chouiniere
		String StudentNameJeffChouiniere = "Jeff Chouiniere";
		studentNames.add(StudentNameJeffChouiniere);
		
		//saving the best for last Adding Danielle Obier
		String StudentNameDanielleObier = "Danielle Obier";
		studentNames.add(StudentNameDanielleObier);
		
		String StudentNameJamie = "Jamie";
		studentNames.add(StudentNameJamie);
		
		//Adding Student: Christopher Minton
		String ChristopherMinton = "Christopher Minton";
		studentNames.add(ChristopherMinton);
		
		String LoganDuck = "Logan Duck";
		studentNames.add(LoganDuck);
		
		//Adding Student: Kenneth Borges profile
		String KennethBorges = "Kenneth Borges";
		studentNames.add(KennethBorges);
		
		//Adding Student: Christian Blosser
		String ChristianBlosser = "Christian Blosser";
		studentNames.add(ChristianBlosser);
		
		//Module 5 Code Assignment
		//Add your name to create a new student profile
		//Use template below and put your name in the areas of 'StudentName'
		//String StudentName3 = "TestStudent3Name";
		//studentNames.add(StudentName3);
		
		return studentNames;
	}

	public Student GetStudentProfile(String student){
		Student emptyStudent = null;
	
		switch(student) {
		   case "AshlyGarner_Playlist":
			   AshlyGarner_Playlist AshlyGarnerPlaylist = new AshlyGarner_Playlist();
			   Student AshlyGarner = new Student("Ashly Garner", AshlyGarnerPlaylist.StudentPlaylist());
			   return AshlyGarner;
			   
		   case "TestStudent2_Playlist":
			   TestStudent2_Playlist testStudent2Playlist = new TestStudent2_Playlist();
			   Student TestStudent2 = new Student("TestStudent2", testStudent2Playlist.StudentPlaylist());
			   return TestStudent2;
			   
		   case "Subash_Playlist":
			   Subash_Playlist subashPlaylist = new Subash_Playlist();
			   Student Subash = new Student("Subash", subashPlaylist.StudentPlaylist());
			   return Subash;
		   
		   case "MatthewMcEntire_PlayList":
			   MatthewMcEntire_PlayList matthewMcEntirePlaylist = new MatthewMcEntire_PlayList();
			   Student matthewMcEntire = new Student("Matthew McEntire", matthewMcEntirePlaylist.StudentPlaylist());
			   return matthewMcEntire;
			   
		   case "TestRobertClark_Playlist":
			   RobertClark_Playlist testRobertClarkPlaylist = new RobertClark_Playlist();
			   Student RobertClark = new Student("Robert Clark", testRobertClarkPlaylist.StudentPlaylist());
			   return RobertClark;
			   
		   case "Jamie_PlayList":
			   Jamie_Playlist jamie_PlayList = new Jamie_Playlist();
			   Student StudentNameJamie = new Student("Jamie", jamie_PlayList.StudentPlaylist());
			   return StudentNameJamie;

		   // Uncomment this after DanielleObier1_Playlist.java is created   
//		   case "DanielleObier1_Playlist":
//				DanielleObier1_Playlist danielleobier1Playlist = new DanielleObier1_Playlist();
//				Student DanielleObier1 = new Student ("Danielle Obier1", DanielleObier1Playlist.StudentPlaylist());
//				return DanielleObier1;
				
			case "ChristopherMinton_Playlist":
				ChristopherMinton_Playlist ChristopherMintonPlaylist = new ChristopherMinton_Playlist();
				Student ChristopherMinton = new Student("Christopher Minton", ChristopherMintonPlaylist.StudentPlaylist());
				return ChristopherMinton;
				
			case "LoganDuck_Playlist":
				return new Student("Logan Duck", new LoganDuck_Playlist().StudentPlaylist());
				
			case "BryceZimbelman_Playlist":
				BryceZimbelman_Playlist bryceZimbelmanPlaylist = new BryceZimbelman_Playlist();
				Student BryceZimbelman = new Student("Bryce Zimbelman", bryceZimbelmanPlaylist.StudentPlaylist());
				return BryceZimbelman;
				
			case "AndrewTroup_Playlist":
				AndrewTroup_Playlist playlist = new AndrewTroup_Playlist();
				Student AndrewTroup = new Student("Andrew Troup", playlist.StudentPlaylist());
				return AndrewTroup;

			case "DavidMcWilliams_Playlist":
				Student DavidMcWilliams = new Student("David McWilliams", new DavidMcWilliams_Playlist().StudentPlaylist());
				return DavidMcWilliams;
				
			case "ChristianBlosser_Playlist":
				ChristianBlosser_Playlist ChristianBlosser_Playlist = new ChristianBlosser_Playlist();
				Student ChristianBlosser = new Student("Christian Blosser", ChristianBlosser_Playlist.StudentPlaylist());
				return ChristianBlosser;

		    //Module 6 Code Assignment - Add your own case statement for your profile. Use the above case statements as a template.
			//Adding Kenneth Borges Playlist case statement.
			case "KennethBorges_Playlist":
				Student KennethBorges = new Student("Kenneth Borges", new KennethBorges_Playlist().StudentPlaylist());
				return KennethBorges;
		}
		
		return emptyStudent;
	}
}
