package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class ChristianBlosser_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> joshTurnerTracks = new ArrayList<Song>();
    JoshTurner joshTurner = new JoshTurner();
	
    joshTurnerTracks = joshTurner.getJoshTurnerSongs();
	
	playlist.add(joshTurnerTracks.get(0));
	playlist.add(joshTurnerTracks.get(1));
	playlist.add(joshTurnerTracks.get(2));
	playlist.add(joshTurnerTracks.get(3));
	playlist.add(joshTurnerTracks.get(4));
	playlist.add(joshTurnerTracks.get(5));
	playlist.add(joshTurnerTracks.get(6));
	
    LynyrdSkynyrd lynyrdSkynyrd = new LynyrdSkynyrd();
	ArrayList<Song> LynyrdSkyryrdTracks = new ArrayList<Song>();
    LynyrdSkyryrdTracks = lynyrdSkynyrd.getLynyrdSkynyrdSongs();
	
	playlist.add(LynyrdSkyryrdTracks.get(0));
	playlist.add(LynyrdSkyryrdTracks.get(1));
	playlist.add(LynyrdSkyryrdTracks.get(2));
	playlist.add(LynyrdSkyryrdTracks.get(3));
	playlist.add(LynyrdSkyryrdTracks.get(4));
	playlist.add(LynyrdSkyryrdTracks.get(5));
	playlist.add(LynyrdSkyryrdTracks.get(6));
	
	//Adding songs from my classmate Kenneth Borge
	KennyChesney KennyChesney = new KennyChesney();
	ArrayList<Song> KennyChesneyTracks = new ArrayList<Song>();
	KennyChesneyTracks = KennyChesney.getKennyChesneySongs();
	
	playlist.add(KennyChesneyTracks.get(0));
	playlist.add(KennyChesneyTracks.get(1));
	playlist.add(KennyChesneyTracks.get(2));
	playlist.add(KennyChesneyTracks.get(3));
	playlist.add(KennyChesneyTracks.get(4));
	playlist.add(KennyChesneyTracks.get(5));
	playlist.add(KennyChesneyTracks.get(6));
	
    return playlist;
	}
}
