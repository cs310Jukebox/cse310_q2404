package snhu.student.playlists;

import java.util.LinkedList;

import music.artist.Adele;
import music.artist.MichaelJackson;
import snhu.jukebox.playlist.PlayableSong;

public class AndrewTroup_Playlist {
	public LinkedList<PlayableSong> StudentPlaylist() {

		LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
		
		// Getting a sub list from the items contained within the songs from the artists.
		playlist.addAll(new Adele().getAdelesSongs().subList(0, 2));
		playlist.addAll(new MichaelJackson().getSongs().subList(0, 3));
		
		return playlist;
	}
}
