package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class Jamie_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> BabymetalTracks = new ArrayList<Song>();
    Babymetal babymetal = new Babymetal();
	
    BabymetalTracks = babymetal.getBabymetalSongs();
	
	playlist.add(BabymetalTracks.get(0));
	playlist.add(BabymetalTracks.get(1));
	playlist.add(BabymetalTracks.get(2));
	
    Nightwish nightwish = new Nightwish();
	ArrayList<Song> NightwishTracks = new ArrayList<Song>();
	NightwishTracks = nightwish.getNightwishSongs();
	
	playlist.add(NightwishTracks.get(0));
	playlist.add(NightwishTracks.get(1));
	playlist.add(NightwishTracks.get(2));
	
	Rammstein rammstein = new Rammstein();
	ArrayList<Song> RammsteinTracks = new ArrayList<Song>();
	RammsteinTracks = rammstein.getRammsteinSongs();
		
	playlist.add(RammsteinTracks.get(0));
	playlist.add(RammsteinTracks.get(1));
	playlist.add(RammsteinTracks.get(2));
	
    return playlist;
	}
}