package snhu.student.playlists;
import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;
public class MatthewMcEntire_PlayList 
{
	public LinkedList<PlayableSong> StudentPlaylist(){
		
		LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
		ArrayList<Song> disturbedTracks = new ArrayList<Song>();
	    Disturbed disturbed  = new Disturbed();
		
	    disturbedTracks = disturbed.GetDisturbedTracks();
		
		playlist.add(disturbedTracks.get(0));
		playlist.add(disturbedTracks.get(1));
		
		
	    ShinyToyGuns shinyToyGuns = new ShinyToyGuns();
		ArrayList<Song> shinyToyGunsTracks = new ArrayList<Song>();
	    shinyToyGunsTracks = shinyToyGuns.GetShinyToyGunsTracks();
		
		playlist.add(shinyToyGunsTracks.get(0));
		playlist.add(shinyToyGunsTracks.get(1));
		playlist.add(shinyToyGunsTracks.get(2));
		
		
		MichaelJackson michaelJackson = new MichaelJackson(); //Added by Matthew McEntire. I love these tracks.
		ArrayList<Song> michaelJacksonTracks = new ArrayList<Song>();
		michaelJacksonTracks = michaelJackson.getSongs();
		
		playlist.add(michaelJacksonTracks.get(0));
		playlist.add(michaelJacksonTracks.get(1));
		playlist.add(michaelJacksonTracks.get(4));
	    return playlist;
		}
}
