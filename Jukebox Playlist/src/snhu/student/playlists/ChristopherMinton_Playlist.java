package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class ChristopherMinton_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> HozierTracks = new ArrayList<Song>();
    Hozier HozierBand = new Hozier();
    
	ArrayList<Song> TheoryOfADeadmanTracks = new ArrayList<Song>();
    TheoryOfADeadman TheoryOfADeadmanBand = new TheoryOfADeadman();
	
    HozierTracks = HozierBand.getHozierSongs();
    TheoryOfADeadmanTracks = TheoryOfADeadmanBand.getTheoryOfADeadmanSongs();
    
	playlist.add(HozierTracks.get(0));
	playlist.add(HozierTracks.get(1));
	playlist.add(TheoryOfADeadmanTracks.get(0));
	playlist.add(TheoryOfADeadmanTracks.get(1));
	playlist.add(TheoryOfADeadmanTracks.get(2));

    return playlist;
	}
}
