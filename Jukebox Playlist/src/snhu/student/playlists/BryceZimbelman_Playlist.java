package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class BryceZimbelman_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	
	ArrayList<Song> ABRTracks = new ArrayList<Song>();					// Setup August Burns Red playlist
    AugustBurnsRed ABRBand = new AugustBurnsRed();
    ABRTracks = ABRBand.getABRSongs();
	
	playlist.add(ABRTracks.get(0));										// Add 3 August Burns Red tracks
	playlist.add(ABRTracks.get(1));
	playlist.add(ABRTracks.get(2));
	
	
    Red redBand = new Red();											// Setup Red playlist
    ArrayList<Song> redTracks = new ArrayList<Song>();
	redTracks = redBand.getRedSongs();
	
	playlist.add(redTracks.get(0));										// Add 2 Red tracks
	playlist.add(redTracks.get(1));
	
	Disturbed disturbed = new Disturbed();								// Setup Disturbed Playlist
	ArrayList<Song> disturbedTracks = new ArrayList<Song>();
	disturbedTracks = disturbed.GetDisturbedTracks();
	
	playlist.add(disturbedTracks.get(0));								// Add 4 Disturbed tracks
	playlist.add(disturbedTracks.get(1));
	playlist.add(disturbedTracks.get(2));
	playlist.add(disturbedTracks.get(3));
	
    return playlist;
	}
}
