package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class Subash_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> nirvanaTracks = new ArrayList<Song>();
    Nirvana nirvana = new Nirvana();
	
    nirvanaTracks = nirvana.getNirvanaSongs();
	
	playlist.add(nirvanaTracks.get(0));
	playlist.add(nirvanaTracks.get(1));
	
	
    TheCranberries theCranberriesBand = new TheCranberries();
	ArrayList<Song> cranberriesTracks = new ArrayList<Song>();
    cranberriesTracks = theCranberriesBand.getCranberriesSongs();
	
	playlist.add(cranberriesTracks.get(0));
	playlist.add(cranberriesTracks.get(1));
	playlist.add(cranberriesTracks.get(2));
	
	//now adding songs from my classmates
	//adding a first song from Christopher Minton's Playlist
	TheoryOfADeadman TheoryOfADeadmanBand = new TheoryOfADeadman();
	ArrayList<Song> TheoryOfADeadmanTracks = new ArrayList<Song>();
    TheoryOfADeadmanTracks = TheoryOfADeadmanBand.getTheoryOfADeadmanSongs();

    playlist.add(TheoryOfADeadmanTracks.get(0));
    
    //adding a second song from Robert Clark's Playlist
    ThieveryCorporation ThieveryCorporationBand = new ThieveryCorporation();
	ArrayList<Song> ThieveryCorporationTracks = new ArrayList<Song>();
    ThieveryCorporationTracks = ThieveryCorporationBand.getThieveryCorporationSongs();
	
	playlist.add(ThieveryCorporationTracks.get(0));
	
	//adding a third song from MatthewMcEntire's Playlist
	ShinyToyGuns shinyToyGuns = new ShinyToyGuns();
	ArrayList<Song> shinyToyGunsTracks = new ArrayList<Song>();
    shinyToyGunsTracks = shinyToyGuns.GetShinyToyGunsTracks();
	
	playlist.add(shinyToyGunsTracks.get(0));
	
    return playlist;
	}
}
