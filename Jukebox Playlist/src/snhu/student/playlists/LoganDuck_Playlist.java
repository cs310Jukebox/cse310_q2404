package snhu.student.playlists;

import java.util.ArrayList;
import java.util.LinkedList;

import music.artist.MichaelJackson;
import music.artist.PartyNextDoor;
import music.artist.RoyWoods;
import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;

/**
 * Creates a playlist of songs featuring Roy Woods, PartyNextDoor, and Michael Jackson.
 * @author LoganDuck
 */
public class LoganDuck_Playlist {
	public LinkedList<PlayableSong> StudentPlaylist() {
		LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
		int index = 0;
		
		RoyWoods royWoods = new RoyWoods();
		ArrayList<Song> royWoodsTracks = royWoods.getSongs();
		// using a while loop to add all songs, in case my taste changes and songs are added or removed.
		while (index < royWoods.getSize()) {
			playlist.add(royWoodsTracks.get(index++));
		}
		
		PartyNextDoor partyNextDoor = new PartyNextDoor();
		ArrayList<Song> partyNextDoorTracks = partyNextDoor.getSongs();
		index = 0;
		while (index < partyNextDoor.getSize()) {
			playlist.add(partyNextDoorTracks.get(index++));
		}
		
		MichaelJackson michaelJackson = new MichaelJackson();
		if (michaelJackson.getSongs().size() >= 1) {
			playlist.add(michaelJackson.getSongs().get(1));
		}
		
		return playlist;
	}
}