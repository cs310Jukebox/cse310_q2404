package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * A collection of David McWilliams's favorite songs
 */
public class DavidMcWilliams_Playlist {

	/**
	 * Returns David McWilliams's favorite songs
	 * 
	 * @return A linked list of playable songs
	 */
	public LinkedList<PlayableSong> StudentPlaylist() {

		// Initialize empty playlist
		LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();

		// Add all Michael Jackson songs
		for (PlayableSong song: new MichaelJackson().getSongs()) {
			playlist.add(song);
		}

		// Add all Nirvana songs
		for (PlayableSong song: new Nirvana().getNirvanaSongs()) {
			playlist.add(song);
		}
		
		// Rickrolled!
		PlayableSong neverGonnaGiveYouUp = new RickAstley().getSongs().get(0);
		playlist.add(neverGonnaGiveYouUp);

		// Add 2 Woodkid songs
		ArrayList<Song> woodkidTracks = new Woodkid().getSongs();
		playlist.add(woodkidTracks.get(0));
		playlist.add(woodkidTracks.get(1));

		return playlist;
	}
}
