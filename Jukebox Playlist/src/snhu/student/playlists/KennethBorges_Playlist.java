package snhu.student.playlists;

import java.util.ArrayList;
import java.util.LinkedList;

import music.artist.KennyChesney;
import music.artist.TraceAdkins;
import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;

//Comment for Peer Review

public class KennethBorges_Playlist {
	public LinkedList<PlayableSong> StudentPlaylist(){
		
		LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
		ArrayList<Song> traceAdkinsTracks = new ArrayList<Song>();
	    TraceAdkins traceAdkinsBand = new TraceAdkins();
		
	    traceAdkinsTracks = traceAdkinsBand.getTraceAdkinsSongs();
		
		playlist.add(traceAdkinsTracks.get(0));
		playlist.add(traceAdkinsTracks.get(1));
		playlist.add(traceAdkinsTracks.get(2));
		
		
	    KennyChesney kennyChesneyBand = new KennyChesney();
		ArrayList<Song> kennyChesneyTracks = new ArrayList<Song>();
	    kennyChesneyTracks = kennyChesneyBand.getKennyChesneySongs();
		
		playlist.add(kennyChesneyTracks.get(0));
		playlist.add(kennyChesneyTracks.get(1));
		playlist.add(kennyChesneyTracks.get(2));
		
	    return playlist;
		}
	}
